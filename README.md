# CI scripts

Repository for shared GitLab CI scripts. 

## Usage

The contained CI scripts can be included in other repositories for CI testing. 

### Test File
The `dax-ci-project.yml` is a script which will spin up the `ARTIQ Master` and the `ARTIQ Controllers Manager` and then run all Python unittests. If this is what the repository needs, then all that needs to be created is a `.gitlab-ci.yml` file which will `include` this script. This is done as shown below:
```yml
include:
  project: 'Duke-ARTIQ/ci-scripts'
  file: '/dax-ci-project.yml'
```

### Customizing Parameters
`dax-ci-project.yml` contains script `test_nix` which automatically runs on CI when included. It is possible to customize parameters within `test_nix`. The example below shows how to override `UNITTEST_CMD` to use a different unittesting command. The original unittest command is `unittest -v`
```yml
test_nix:
  variables:
    UNITTEST_CMD: "pytest -v"
```

Relevant documentation may be found here: [Overriding external template values](https://docs.gitlab.com/ee/ci/yaml/includes.html#overriding-external-template-values)

### Environments File
If the project needs more customizable test code, then consider extending the included environment files. `Including` `envs.yml` will allow the CI script to use the contents of `envs.yml`, such as by extending the `.nix_env` environment fur running tests, shown below:
```yml
include:
  project: 'Duke-ARTIQ/ci-scripts'
  file: '/envs.yml'

test_nix:
  extends: .nix_env
```

The `.conda_env` requires that the local repository specifies a yaml conda environment file. The name of the environment file must be overridden when extending the `.conda_env`. Please see the Conda documentation on creating environment files in the `.yml` format for further help.

```yml
include:
  project: 'Duke-ARTIQ/ci-scripts'
  file: '/envs.yml'

test_conda:
  extends: .conda_env
  variables:
    CONDA_ENV_FILE: "test/environment.yml"
```

## Resources

[GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)

[More `Include` Examples](https://docs.gitlab.com/ee/ci/yaml/includes.html)
